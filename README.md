# Hello Cordova

Yet another Hello World application in Cordova. We used Bootstrap, jQuery, and JsRender in this app.

## Copyright.

2018, Michael Chen; Apache 2.0
